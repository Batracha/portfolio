# Portfolio
Tutaj znajdują się linki do moich projektów.

## Projekty:
- [ ] [Baza SQL na trzeci semestr](https://gitlab.com/batracha1/studenckie/baza-sql-na-trzeci-semestr)
- [ ] [Gra-labirynt, Linux bash na drugi semestr](https://gitlab.com/batracha1/studenckie/Linux-bash-pierwszy-rok)
- [ ] [Kalkulator dużych liczb C++](https://gitlab.com/batracha1/studenckie/C_dodawanie-w-napisach-trzeci-semestr)
- [ ] [Przelicznik liczb rzymskich/arabskich C++](https://gitlab.com/batracha1/studenckie/C_cyfry-rzymskie-i-arabskie-3-semestr)
- [ ] [Java na czwarty semestr, 1](https://gitlab.com/batracha1/studenckie/Java-dziedziczenie-geometria-czwarty-semestr)
- [ ] [Java na czwarty semestr, 2](https://gitlab.com/batracha1/studenckie/Java-dziedziczenie-zielnik-czwarty-semestr)
- [ ] [Python na trzeci semestr](https://gitlab.com/batracha1/studenckie/Python-zadanie-na-trzeci-semestr)
- [ ] [XML na trzeci semestr](https://gitlab.com/batracha1/studenckie/XML-zadanie-na-trzeci-semestr)
- [ ] [PHP na drugi semestr](https://gitlab.com/batracha1/studenckie/PHP-zadanie-na-drugi-semestr)
